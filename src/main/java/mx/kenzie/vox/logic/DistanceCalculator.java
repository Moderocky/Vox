package mx.kenzie.vox.logic;

import java.util.Arrays;

class DistanceCalculator {

    static int calculate(String x, String y) {
        byte[][] dp = new byte[x.length() + 1][y.length() + 1];
        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = (byte) j;
                } else if (j == 0) {
                    dp[i][j] = (byte) i;
                } else {
                    dp[i][j] = min(dp[i - 1][j - 1] + subCost(x.charAt(i - 1), y.charAt(j - 1)),
                            dp[i - 1][j] + 1,
                            dp[i][j - 1] + 1);
                }
            }
        }
        return dp[x.length()][y.length()];
    }

    static byte subCost(char a, char b) {
        return (byte) (a == b ? 0 : 1);
    }

    static byte min(int... numbers) {
        return (byte) Arrays.stream(numbers).min().orElse(Byte.MAX_VALUE);
    }

}
