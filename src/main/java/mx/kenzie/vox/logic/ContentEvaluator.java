package mx.kenzie.vox.logic;

import com.moderocky.mask.api.MagicList;
import com.moderocky.mask.api.MagicStringList;
import mx.kenzie.vox.Vox;
import mx.kenzie.vox.config.VoxConfig;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContentEvaluator {

    private final VoxConfig config = Vox.<Vox>getInstance().config;
    private final MagicStringList lenient = new MagicStringList(Vox.<Vox>getInstance().config.lenient);
    private final MagicStringList strict = new MagicStringList(Vox.<Vox>getInstance().config.strict);
    private final MagicList<Pattern> lenientPatterns = new MagicList<>(lenient.collect(string -> Pattern.compile("(" + string + ")")));
    private final MagicList<Pattern> strictPatterns = new MagicList<>(strict.collect(string -> Pattern.compile("(" + string + ")")));

    public ContentEvaluator() {
    }

    public EvaluationResult evaluate(String input) {
        return new EvaluationResult(input);
    }

    public String star(String string, Collection<String> entries) {
        for (String entry : entries) {
            if (!string.contains(entry)) continue;
            string = string.replace(entry, getStars(entry));
        }
        return string;
    }

    private String getStars(String string) {
        String[] parts = string.split("");
        int i = 0;
        for (String part : new MagicStringList(parts)) {
            if (!part.equals(" ")) parts[i] = "*";
            i++;
        }
        return String.join("", parts);
    }

    public enum Result {
        PASS, SOFT_FAIL, HARD_FAIL
    }

    public class EvaluationResult {

        public final MagicStringList lenientCatchments = new MagicStringList();
        public final MagicStringList strictCatchments = new MagicStringList();
        private final String input;
        private final String[] words;
        private Result result = Result.PASS;

        private EvaluationResult(String input) {
            this.input = input.toLowerCase();
            this.words = input.toLowerCase().split(" ");
            evaluate();
        }

        private void evaluate() {
            for (String word : words) {
                for (String string : lenient) {
                    if (!word.matches(string)) continue;
                    if (!lenientCatchments.containsIgnoreCase(word)) lenientCatchments.add(word);
                    result = Result.SOFT_FAIL;
                }
            }
            for (Pattern pattern : lenientPatterns) {
                Matcher matcher = pattern.matcher(input);
                while (matcher.find()) {
                    String string = matcher.group();
                    if (!lenientCatchments.containsIgnoreCase(string)) lenientCatchments.add(string);
                    result = Result.SOFT_FAIL;
                }
            }
            for (Pattern pattern : strictPatterns) {
                Matcher matcher = pattern.matcher(input);
                while (matcher.find()) {
                    String string = matcher.group();
                    if (!strictCatchments.containsIgnoreCase(string)) strictCatchments.add(string);
                    result = Result.HARD_FAIL;
                }
            }
            for (String word : words) {
                for (String string : strict) {
                    if (!word.equalsIgnoreCase(string) && !word.contains(string)) continue;
                    if (!strictCatchments.containsIgnoreCase(string)) strictCatchments.add(string);
                    result = Result.HARD_FAIL;
                }
            }
            for (String word : words) {
                for (String catchment : strict) {
                    if (DistanceCalculator.calculate(word, catchment) > config.strictLevensteinDistance) continue;
                    if (!strictCatchments.containsIgnoreCase(word)) strictCatchments.add(word);
                    result = Result.HARD_FAIL;
                }
            }
        }

        public Result getResult() {
            return result;
        }

        public String getInput() {
            return input;
        }

        public String[] getWords() {
            return words;
        }
    }

}
