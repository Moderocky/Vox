package mx.kenzie.vox.config;

import com.moderocky.mask.annotation.Configurable;
import com.moderocky.mask.api.MagicStringList;
import com.moderocky.mask.template.Config;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class VoxConfig implements Config {

    @Configurable
    public List<String> lenient = new MagicStringList("lorem", "ipsum", "dol[o0]r", "s[i1]t", "am[e3]ts?");

    @Configurable
    public List<String> strict = new MagicStringList("consectetur", "adipiscings?", "eli*t", "ett?i[a4]m");

    @Configurable
    public String blockMessage = "Blocked words were identified in your message:";

    @Configurable("colours")
    public String harsh = "#fc5603";

    @Configurable("colours")
    public String medium = "#fcba03";

    @Configurable("colours")
    public String soft = "#ffed63";

    @Configurable
    public int strictLevensteinDistance = 2;

    @Override
    public @NotNull String getFolderPath() {
        return "plugins/Vox/";
    }

    @Override
    public @NotNull String getFileName() {
        return "config.yml";
    }
}
