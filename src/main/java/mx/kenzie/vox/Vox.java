package mx.kenzie.vox;

import com.moderocky.mask.template.BukkitPlugin;
import mx.kenzie.vox.config.VoxConfig;
import mx.kenzie.vox.listener.AsyncPlayerChatListener;
import org.jetbrains.annotations.NotNull;

public final class Vox extends BukkitPlugin {

    public final @NotNull VoxConfig config = new VoxConfig();

    @Override
    public void startup() {
        config.load();
        register(new AsyncPlayerChatListener());
    }

    @Override
    public void disable() {

    }

}
