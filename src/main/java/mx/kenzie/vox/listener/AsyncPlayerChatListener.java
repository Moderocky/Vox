package mx.kenzie.vox.listener;

import com.moderocky.mask.template.Listener;
import mx.kenzie.vox.Vox;
import mx.kenzie.vox.config.VoxConfig;
import mx.kenzie.vox.logic.ContentEvaluator;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener<AsyncPlayerChatEvent> {

    final ContentEvaluator evaluator;
    final VoxConfig config;

    {
        this.evaluator = new ContentEvaluator();
        this.config = Vox.<Vox>getInstance().config;
    }

    @Override
    @EventHandler
    public void event(AsyncPlayerChatEvent event) {
        if (event.isCancelled()) return;
        String message = event.getMessage().toLowerCase();
        ContentEvaluator.EvaluationResult result = evaluator.evaluate(message);
        if (result.getResult() == ContentEvaluator.Result.PASS) return;
        else if (result.getResult() == ContentEvaluator.Result.SOFT_FAIL) {
            message = evaluator.star(message, result.lenientCatchments);
            message = evaluator.star(message, result.strictCatchments);
            event.setMessage(message);
        } else {
            event.setCancelled(true);
            String sample = ChatColor.stripColor(message);
            for (String catchment : result.strictCatchments) {
                sample = sample.replace(catchment, ChatColor.of(config.harsh) + catchment + "§r");
            }
            for (String catchment : result.lenientCatchments) {
                sample = sample.replace(catchment, ChatColor.of(config.soft) + catchment + "§r");
            }
            event.getPlayer().spigot().sendMessage(new ComponentBuilder()
                    .append(config.blockMessage).color(ChatColor.of(config.medium))
                    .append("\n").retain(ComponentBuilder.FormatRetention.NONE)
                    .append("\"").color(ChatColor.GRAY)
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder()
                            .append("Highlighted Sections:")
                            .append("\n")
                            .append(String.join(", ", result.strictCatchments))
                            .strikethrough(true)
                            .color(ChatColor.of(config.harsh))
                            .append("\n")
                            .append(String.join(", ", result.lenientCatchments))
                            .strikethrough(true)
                            .color(ChatColor.of(config.soft))
                            .create()))
                    .append(TextComponent.fromLegacyText(sample, ChatColor.GRAY))
                    .append("\"").color(ChatColor.GRAY)
                    .create());
        }
    }

}
